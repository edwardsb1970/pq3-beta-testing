# PQ3 Beta-Testing

Repository for videos of puzzlequest 3 testing and/or bugs detected.

Access specifically for developers at InfinityPlus2 .

However, as a showcase to you who stumbled across this project, contact me at ceo@pf4edward.biz to book a consultation or hire me for a specific beta-testing session.  Fees charged are negotiable.

-Edward S.B.
ceo@pf4edward.biz
